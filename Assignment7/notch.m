function [NF, nImg, fft_img] = notch(img)
% Try to create ideal notch pass filter along the horizontal axis of the F domain
% NF - the filter
% nImg - notch filtered image
% fft_img - Centered Fourier spectrum of the image

% Inspect the spectrum to see where the noise might be happening
fft_img = fftshift(fft2(img));
figure; imshow(mat2gray(20*log10(abs(fft_img))));
title('Spectrum of the image');
imwrite(mat2gray(20*log10(abs(fft_img))), 'F_spectrum.png');

% Create the rejection zone and set the horizontal to 1 
% the location of this depends on the size of the image
NF = zeros(size(img));
NF(100:126, :) = 1;
figure; imshow(NF);
title('Horizontal notch pass filter at the center ');
imwrite(NF, 'NPfilter.png');

% Filter the image and ifft
fft_filt = fft_img .* NF;
nImg = ifft2(fft_filt);


end


