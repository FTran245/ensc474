%% ========================= Setup ========================= %%
%% ========================= Setup ========================= %%
% This script will attempt to remove noise from an image corrupted with
% both salt and pepper noise, and periodic noise
% First we will use a median filter for the salt and pepper noise, then a
% notch filter in the Fourier domain for the periodic noise

% Read image
img = im2double(imread('SaturnRingsWithRandomNoise.png'));

%% ========================= Median Filter ========================= %%
% To remove salt and pepper noise, use median filter
% Apply a 5x5 median filter (arbitrarily chosen)
medIm = medFilt7(img, 5);
figure; imshow(medIm);
title('Median Filtered Image');
imwrite(medIm, 'MedImg.png');

%% ========================= Notch Filter ========================= %%
% To remove periodic noise, we consider the notch filter
% We will use a notch pass filter to only pass the frequencies of the
% image, and not the frequencies of the noise

% Notch pass filter the image
[NF, nImg, fft_img] = notch(medIm);
figure; imshow(mat2gray(abs(nImg)));
imwrite(mat2gray(abs(nImg)), 'notchedImg.png');

% To show the noise, we can do a notch reject filter
notchreject = 1 - NF;
figure; imshow(notchreject);
title('Notch reject filter');
imwrite(notchreject, 'NRfilter.png');

% Multiply in the Fourier domain and inverse transform
a = fft_img .* notchreject;
noiseImg = ifft2(a);

figure; imshow(mat2gray(20*log10(abs(noiseImg))));
title('Noise removed by the notch filter');
imwrite(mat2gray(20*log10(abs(noiseImg))), 'pNoise.png');



