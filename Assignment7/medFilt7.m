function medianImg = medFilt(SP, n)
% Returns image as a median filtered image with a specific median filter
% SP = input image
% n = size of median filter (n x n)

% The padding size will be the size of the filter divided by 2, rounded
% down
pSize = floor(n/2);

% Zero pad image
padImg = padarray(SP, [pSize, pSize], 0, 'both');

% Find the size of the input image and create an image buffer based on the
% size
r = size(SP, 1);
c = size(SP, 2);
medianImg = zeros(size(SP));

% For all pixels in the image, run through and "window" the median
for y = 1:r
    for x = 1:c
        
        % Find all elements in the nxn window, return the median of those
        % elements
        mask  = padImg(y:y+n-1, x:x+n-1);
        medianImg(y, x) = median(mask, 'all');
        
    end
end


end