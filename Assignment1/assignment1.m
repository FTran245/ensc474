warning off
addpath(genpath(pwd));

%% ========================= Question 1 ========================= %%
img = displayImage('selfie.jpg');



%% ========================= Question 2 ========================= %%
%convert the image to grayscale
img_gr = colour2gray(img);




%% ========================= Question 3 ========================= %%
%create x and y values within the specified ranges
xi = 200:(1/pi):455;
yi = 50:(1/pi):305;

sincPlot(xi, yi);




