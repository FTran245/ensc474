function img_gr = colour2gray(img)

%extract RGB components
R = img(:, :, 1);
G = img(:, :, 2);
B = img(:, :, 3);

%average and display
img_gr = (R + G + B) / 3;
figure;
imshow(img_gr);
title('Figure Question 2.1: grayscaled image');

end