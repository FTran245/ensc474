function img = displayImage(filename)

%read in and display image
img = imread(filename);
figure;
imshow(img);
title('Figure Question 1.1: Colour image');

end