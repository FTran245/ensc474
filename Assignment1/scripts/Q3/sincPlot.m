function sincPlot(xi, yi)

%create x and y values within the specified ranges
% xi = 200:(1/pi):455;
% yi = 50:(1/pi):305;

%%create a meshgrid
[x, y] = meshgrid(xi, yi);

%%create the sinc function with r as a circle
%%offset the center of the sinc function and scale to look presentable
xOffset = 300;
yOffset = 150;
r = sinc((1/20)*sqrt((x - xOffset).^2 + (y - yOffset).^2));

%plot 3D sinc function
figure;
grid on;
s = surf(x, y, r);
s.EdgeColor = 'none';
title('Figure Question 3.1, 3D sinc surface plot');
xlabel('x axis');
ylabel('y axis');

%%plot Cartesian grayscale sinc function
figure;
imagesc(r); colormap(gray);
title('Figure Question 3.2, Cartesian grayscale sinc plot');
xlabel('x axis');
ylabel('y axis');
axis xy


end

