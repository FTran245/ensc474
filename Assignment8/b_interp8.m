function fx = b_interp8(im, x, y)
% this function takes an image im, and given two positive coordinates (integer or
% non integer) will return the interpolated value at that point

%        f3---------(1-a)-f4
%                |        |
%      1-b       |        |
%                |        |
%         -----------------
%                |        |
%       b        |        |
%                |        |
%        f1--a----(1-a)---f2

% floor the coordinates, then use those as bases for interpolation
% note that in Matlab, the x, y coordinates are 'reveresed'
% x = columns y = rows

% identify the four points surrounding the subpixel location (x, y)
% and calculate the actual values from the given image
x11 = floor(x);
x12 = floor(x);
x21 = ceil(x);
x22 = ceil(x);

y11 = floor(y);
y12 = ceil(y);
y21 = floor(y);
y22 = ceil(y);
% 
% boundary conditions
if (x > size(im, 2) || y > size(im, 1))
    
    f1 = 0;
    f2 = 0;
    f3 = 0;
    f4 = 0;
    
elseif (abs(x11) < 1 || abs(y11) < 1)
    f1 = 0;
    f2 = im(y22, x22);
    f3 = 0;
    f4 = 0;
    
elseif(y21 < 1 || x21 < 1)
    
    f1 = im(y12, x12);
    f2 = 0;
    f3 = 0;
    f4 = 0;
    
elseif(y22 < 1 || x22 < 1)
    
    f1 = 0;
    f2 = 0;
    f3 = im(y11, x11);
    f4 = 0;

else
    % actual data points
    f1 = im(y12, x12);
    f2 = im(y22, x22);
    f3 = im(y11, x11);
    f4 = im(y21, x21);

end


% alpha and beta are the fractional parts of the pixel coordinates
% alpha and (1-alpha)
a = x - x11;
b = y - y11;
% an = 1 - (x - xim);
% bn = 1 - (y - yim);

% interpolate x first
% f3 and f4
R1 = a*double(f2) + (1-a)*double(f1);
R2 = a*double(f4) + (1-a)*double(f3);

% now interpolate y
% this will be the value of the interpolated point at the given (x, y)
fx = b*R2 + (1-b)*R1;


end