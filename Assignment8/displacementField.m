function [ux_x, ux_y] = displacementField(img, ui, fixed, var)
% img - the image
% ui - the displacements of the landmarks
% fixed - the fixed landmarks (p from lecture)
% var - variance
% returns  the displacement field of all (x, y) coordinates in (ux_x, ux_y)

% Initialize displacement buffers
ux_x = zeros(size(img));
ux_y = ux_x;

for y = 1:size(img, 1)
    for x = 1:size(img, 2)
        % x and y displacements respectively
        a = exp( -sum(([x, y] - fixed).^2, 2) ./ (2*var^2) ); % Weight factor
        ux_x(y, x) = sum(a.*ui(:, 1), 1); % x y are different from cpselect...
        ux_y(y, x) = sum(a.*ui(:, 2), 1);
    end
end


end