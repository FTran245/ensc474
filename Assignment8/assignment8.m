%% ========================= Setup ========================= %%
%% ========================= Setup ========================= %%
% This script will use cpselect to set landmarks on a target and template
% image which will be used to non rigidly register one image to another
template = im2double(rgb2gray(imread('template.jpg')));
target = im2double(rgb2gray(imread('target.jpg')));
savepath = 'files';

if ~exist(savepath, 'dir')
    mkdir(savepath);
end

% Resize for display
template = imresize(template, [size(template, 1)/2, size(template, 2)/2]);
target = imresize(target, [size(target, 1)/2, size(target, 2)/2]);
imwrite(template,fullfile(savepath, 'temp.png'));
imwrite(target, fullfile(savepath, 'tar.png'));

%% ========================= Question 1 ========================= %%
% Use cpselect, choose 5-10 landmarks on the template and target 
% Estimate the displacement vector u(x) everywhere in the image domain using
% displacements at these chosen landmarks
figure; montage(cat(3, template, target), 'size', [1, 2]);
title('Q1: Left: Template; Right: Target');

% cpselect(moving, fixed);
% Use the 'wait' option to pause the command line
% Select in (p, q) pairs, one point on template, then one on moving, then
% back to template, the moving then etc..
% In order to save time, I have pre-set the landmarks and saved them in
% variables called fixed.mat and moving.mat to be loaded. All you need to
% do is click the exit button
load('fixed.mat');
load('moving.mat');
[moving, fixed] = cpselect(target, template, initialMovingPoints, initialFixedPoints, 'wait', true);
% save('moving.mat', 'initialMovingPoints', '-v7.3');
% save('fixed.mat', 'initialFixedPoints', '-v7.3');

% Landmark displacements ui = q - p
% cpselect will return (cols, rows), NOT (rows, cols)
ui = moving - fixed;

% Construct a displacement field for all points x over all landmarks
var = 12; % YOU can choose var!
[ux_x, ux_y] = displacementField(template, ui, fixed, var);

% Estimate the transformation grid
[ph_x, ph_y, iph_x, iph_y] = createTransformationGrid(template, ux_x, ux_y);

% The quiver plot is upside down, so you need to flip it to see it
% correctly ..I think
figure; quiver(flip(ux_y(1:2:end, 1:2:end), 1), flip(ux_x(1:2:end, 1:2:end), 1), 0.8);
title('Q1: Quiver plot non-smile to smile');

% Make the unsmiling face smile
I2 = zeros(size(template));
for y = 1:size(template, 1)
    for x = 1:size(template, 2)
        I2(y, x) = template(round(iph_y(y, x)), round(iph_x(y, x)));
    end
end

figure; imshow(I2);
title('Q1: Non-smile to Smile');
imwrite(I2, fullfile(savepath, 'Temp_to_Tar.png'));

% Make the smiling face unsmile
% You will need interpolation for this
I1 = zeros(size(template));
for y = 1:size(template, 1)
    for x = 1:size(template, 2)
        I1(y, x) = b_interp8(target, ph_x(y, x), ph_y(y, x));
    end
end

figure; imshow(I1);
title('Q1: Smile to Non-smile');
imwrite(I1, fullfile(savepath, 'Tar_to_Temp.png'));


%% ========================= Question 2 ========================= %%
% Place landmarks on the template to generate a frown
% Do the same thing as Question 1
clear x; clear y

% cpselect(moving, fixed);
% Use the 'wait' option to pause the command line
% Select in (p, q) pairs, one point on template, then one on moving, then
% back to template, the moving then etc..
load('fixed2.mat');
load('moving2.mat');
[moving2, fixed2] = cpselect(target, template, initialMovingPoints2, initialFixedPoints2, 'wait', true);
% save('moving2.mat', 'initialMovingPoints2', '-v7.3');
% save('fixed2.mat', 'initialFixedPoints2', '-v7.3');


% Landmark displacements
% cpselect will return (cols, rows), NOT (rows, cols)
ui2 = moving2 - fixed2;
var2 = 12;

% Create the displacement field u(x) for each point x
[ux_x2, ux_y2] = displacementField(template, ui2, fixed2, var2);

% Create the forward/backward transformation fields
[ph_x2, ph_y2, iph_x2, iph_y2] = createTransformationGrid(template, ux_x2, ux_y2);

% Forward frown
I_frown = zeros(size(template));
for y = 1:size(template, 1)
    for x = 1:size(template, 2)
        I_frown(y, x) = template(round(iph_y2(y, x)), round(iph_x2(y, x)));
    end
end

figure; imshow(I_frown);
title('Q2: Frown');
imwrite(I_frown, fullfile(savepath, 'frown.png'));


% Backward frown
I_unfrown = size(template);
for y = 1:size(template, 1)
    for x = 1:size(template, 2)
        I_unfrown(y, x) = b_interp8(I_frown, ph_x2(y, x), ph_y2(y, x));
    end
end

% Quiver plot
figure; quiver(flip(ux_y2(1:2:end, 1:2:end), 1), flip(ux_x2(1:2:end, 1:2:end), 1), 0.8);
title('Q2: Quiver plot for frowning');

% Unfrown display, for some reason this won't run..
disp('testing');
figure; imshow(I_unfrown);
title('Q2: Unfrown');
imwrite(I_unfrown, fullfile(savepath, 'unfrown.png'));


%% ========================= Question 3 ========================= %%
% Create an animation of Questions 1 and 2
% ph(x) = x + n(1/T)u(x) for T steps
% T = number of steps
% n = the n'th step

T = 5; % Number of steps

% Initialize variables
I_forwardSmile = zeros([size(template), T]);
I_backSmile = I_forwardSmile;
I_forwardFrown = I_forwardSmile;
I_backFrown = I_forwardSmile;

% For the number of steps, create an partially transformed image for that step
for n = 1:T
    for y = 1:size(template, 1)
        for x = 1:size(template, 2)
            % Forward smile transformation
            iph_x(y, x, n) = x - n*(1/T)*ux_x(y, x);
            iph_y(y, x, n) = y - n*(1/T)*ux_y(y, x);
            I_forwardSmile(y, x, n) = template(round(iph_y(y, x, n)), round(iph_x(y, x, n)));
            
            % Backward smile transformation
            ph_x(y, x) = x + n*(1/T)*ux_x(y, x);
            ph_y(y, x) = y + n*(1/T)*ux_y(y, x);
            I_backSmile(y, x, n) = b_interp8(target, ph_x(y, x), ph_y(y, x));
            
            % Forward frown transformation
            iph_x2(y, x) = x - n*(1/T)*ux_x2(y, x);
            iph_y2(y, x) = y - n*(1/T)*ux_y2(y, x);
            I_forwardFrown(y, x, n) = template(round(iph_y2(y, x)), round(iph_x2(y, x)));
            
            % Backward frown transformation
            ph_x2(y, x) = x + n*(1/T)*ux_x2(y, x);
            ph_y2(y, x) = y + n*(1/T)*ux_y2(y, x);
            I_backFrown(y, x, n) = b_interp8(I_frown, ph_x2(y, x), ph_y2(y, x));
        end
    end
end

% Create the GIF volume
gif = cat(3, I_forwardSmile, I_backSmile, I_forwardFrown, I_backFrown);

figure;
% Display the GIF volume
for i = 1:20
   imshow(gif(:, :, i));
   title('Q3: GIF');
   pause(0.3);
end

figure; montage(gif, 'size', [5, 4]);
title('Q3: All the expressions from unsmile to smile');


% Write a GIF file lol
% gray2ind turns a grayscale image to an indexed image
% Returns X, the indexed image, and cmap its colormap
% To write a GIF you need to give it an indexed image
% LoopCount field tells how many times to repeat the image
for jj = 1:size(gif, 3)
    if (jj == 1)
        [X, cmap] = gray2ind(gif(:, :, jj));
        imwrite(X, cmap,fullfile(savepath, 'myGIF.gif'), 'gif','LoopCount', Inf, 'DelayTime', 0.3);
    else
        [X, cmap] = gray2ind(gif(:, :, jj));
        imwrite(X, cmap,fullfile(savepath, 'myGIF.gif'), 'gif','WriteMode', 'append', 'DelayTime', 0.3);
    end
end




