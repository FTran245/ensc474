function [ph_x, ph_y, iph_x, iph_y] = createTransformationGrid(img, ux_x, ux_y)
% Creates a transformation grid out of the image, and the displacement
% fields ux_x, and ux_y which correspond to (x, y) displacements

% Initialize transformation buffers
ph_x = zeros(size(img));
ph_y = ph_x;
iph_x = ph_x;
iph_y = ph_x;

% Create the forward and backward transformation grids
for y = 1:size(img, 1)
    for x = 1:size(img, 2)
        ph_x(y, x) = x + ux_x(y, x);
        ph_y(y, x) = y + ux_y(y, x);
        
        iph_x(y, x) = x - ux_x(y, x);
        iph_y(y, x) = y - ux_y(y, x);
    end
end


end