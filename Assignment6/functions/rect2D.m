function r = rect2D(M, N, a, b)
% Creates a 2D rect function of dimensions MxN 
% 1 if (-a < x < a) AND (-b < y < b) and 0 elsewhere

% Initialize buffer
r = zeros(M, N);

% Find lower and upper bounds of (x,y) for the 2D rect
% r = 1 if (-a < x < a) AND (-b < y < b)
ylow = round(M/2 - a);
yhi = round(M/2 + a);
xlow = round(N/2 - b);
xhi = round(N/2 + b);

r(ylow:yhi, xlow:xhi) = 255;


end