function I = applyFilter6(img, mask)
% This function applies a mask to img and returns the result to I

% Determine pad size
% It will be size of mask divided by 2, rounded up
r_pad = floor(size(mask, 1)/2);
c_pad = floor(size(mask, 2)/2);

% Zero pad image 
im = padarray(img, [r_pad, c_pad], 0, 'both');

% Find number of rows and columns
r = size(img, 1); 
c = size(img, 2);

% Initialize image buffer based on original image size
I = zeros(size(img));

% For all pixels in the unpadded image!
for y = 1:r
   for x = 1:c 
       
       % For all the pixels in the mask!
       % Perform the convolution over the mask summing the products of each
       % element in the mask accumulating them in pixel (x, y) for all
       % pixels
       for i = 1:size(mask, 1)
          for j = 1:size(mask, 2)
              I(y, x) = I(y, x) + mask(i, j) * im(y + i - 1, x + j - 1);
          end
       end
       
   end
end


end
