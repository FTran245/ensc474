function n = cos2D(M, N, u0, v0)
% Returns a 2D cosine function 

% Cosine function in 2D
xi = 0:N-1;
yi = 0:M-1;
[x, y] = meshgrid(xi, yi);

wx = 2*pi/M;
wy = 2*pi/N;

% Add 1 to cosine to limit it between [0, 2], then multiply by 255/2 to
% achieve bounds between [0, 255]
co = cos((u0*x*wx) + (v0*y*wy)) + 1;
n = (255/2)*co;

end