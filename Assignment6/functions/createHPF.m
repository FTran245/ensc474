function HPF = createHPF(img, C)
% Creates an ideal 2D frequency high pass filter with "rejection" diameter
% C

nlow = -round(size(img, 1)/2);
nhi = round(size(img, 1)/2);

mlow = -round(size(img, 2)/2);
mhi = round(size(img, 2)/2);

[x, y] = meshgrid(mlow:mhi, nlow:nhi);

% Create the circle
z = sqrt(x.^2 + y.^2);

% Set D, the diameter of the HPF
% Instead of the "acceptance" circle, this is the "rejection" circle
HPF = z > C;
HPF = imresize(HPF, size(img));

end