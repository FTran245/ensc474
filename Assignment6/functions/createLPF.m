function LPF = createLPF(img, D)
% Creates an ideal 2D frequency low pass filter with diameter D

nlow = -round(size(img, 1)/2);
nhi = round(size(img, 1)/2);

mlow = -round(size(img, 2)/2);
mhi = round(size(img, 2)/2);

[x, y] = meshgrid(mlow:mhi, nlow:nhi);

% Create the circle
z = sqrt(x.^2 + y.^2);

% Set D, the diameter of the LPF
LPF = z < D;
LPF = imresize(LPF, size(img));

end