%% ========================= Setup ========================= %%
%% ========================= Setup ========================= %%
% This script will create approximately 31 figures. I have already tried to
% minimize the number of figures by using montage(), but there's just too
% many things to display in this assignment
% Lol sorry Ashley

% Setting up directories
addpath(genpath(pwd));

I = im2double(rgb2gray(imread('selfie.jpg')));
img = imresize(I, [size(I, 1)/3, size(I, 2)/3]);
figure; imshow(img);
title('Original Image');

%% ========================= Question 2 ========================= %%
% Remove Gaussian noise using a 'constant' mask e.g. a mask whose values
% all add up to 1

% Mean and variance
M = 0.2;
V = 0.05;

% Add noise to to the image and display
J = imnoise(img, 'gaussian', M, V);
figure; imshow(J);
title('Q2: Image with Gaussian Noise (M = 0.2, V = 0.05)');

% Define constant masks, apply them with convolution, and display the
% results

% Initialize image volume buffer (for easier display, unless you want
% many figure windows)
imgBuf= zeros(size(img, 1), size(img, 2), 3);

% 5x5 mask
m5 = (1/25)*ones(5, 5);
imgBuf(:, :, 1) = applyFilter6(J, m5);

% 10x10 mask
m10 = (1/100)*ones(10, 10);
imgBuf(:, :, 2) = applyFilter6(J, m10);

% 20x20 mask
m20 = (1/400)*ones(20, 20);
imgBuf(:, :, 3) = applyFilter6(J, m20);

figure; montage(imgBuf, 'Size', [1, 3]);
title('Q2: Noisey image mean filtered');


%% ========================= Question 3 ========================= %%
% Create a median filter and use it to filter salt+pepper noise

% Add noise to image
% Density (between 0 and 1)
d1 = 0.1;
d6 = 0.6;

SP1 = imnoise(img, 'salt & pepper', d1);
figure; imshow(SP1);
title('Q3: Salt and Pepper Image D = 0.1');

SP8 = imnoise(img, 'salt & pepper', d6);
figure; imshow(SP8);
title('Q3: Salt and Pepper Image D = 0.6');

% Initialize more image volume buffers for display
medImages = zeros(size(img, 1), size(img, 2), 6);

% Lower densities (d = 0.1)
medImages(:, :, 1) =  medFilt(SP1, 3);
medImages(:, :, 2) =  medFilt(SP1, 5);
medImages(:, :, 3) =  medFilt(SP1, 7);

% Higher densities (d = 0.8)
medImages(:, :, 4) =  medFilt(SP8, 3);
medImages(:, :, 5) =  medFilt(SP8, 5);
medImages(:, :, 6) =  medFilt(SP8, 7);

figure; montage(medImages, 'Size', [2, 3]);
title('Q3: Median Filtered Images with N = 3, 5, 7');

% Median filter vs Gaussian Noise %
MvG(:, :, 1) = medFilt(J, 5);
MvG(:, :, 2) = medFilt(J, 10);
MvG(:, :, 3) = medFilt(J, 20);

figure; montage(MvG, 'Size', [1, 3]);
title('Q3: Median filter vs Gaussian Noise with N = 5, 10, 20');

% Mean filter vs salt+pepper noise %

% For D = 0.1
MvS(:, :, 1) = applyFilter6(SP1, m5);
MvS(:, :, 2) = applyFilter6(SP1, m10);
MvS(:, :, 3) = applyFilter6(SP1, m20);

% For D = 0.6
MvS(:, :, 4) = applyFilter6(SP8, m5);
MvS(:, :, 5) = applyFilter6(SP8, m10);
MvS(:, :, 6) = applyFilter6(SP8, m20);

figure; montage(MvS, 'Size', [2, 3]);
title('Q3: Mean Filter vs Salt and Pepper Noise with N = 5, 10, 20');

%% ========================= Question 4 ========================= %%
% Create a 2D cosine function and display with different parameter values
M = size(img, 1);
N = size(img, 2);

% All different combos
u0 = 0; 
v0 = 0;

u1 = M/4;
v1 = 0;

u2 = 0; 
v2 = N/2;

u3 = M/2; 
v3 = N/4;

% Create the cosine functions
n0 = cos2D(M, N, u0, v0);
n1 = cos2D(M, N, u1, v1);
n2 = cos2D(M, N, u2, v2);
n3 = cos2D(M, N, u3, v3);

% For montaging
cosBuf = cat(3, n0, n1, n2, n3);
figure; montage(mat2gray(cosBuf), 'size', [1, 4]);

% imwrite(cosBuf(:, :, 1), 'cos0.tif');
% imwrite(cosBuf(:, :, 2), 'cos1.tif');
% imwrite(cosBuf(:, :, 3), 'cos2.tif');
% imwrite(cosBuf(:, :, 4), 'cos3.tif');

%% ========================= Question 5 ========================= %%
% Create 2D rect function and display

% Choose your own dimensions
M = size(img, 1);
N = size(img, 2);

% Size of the rect, cannot exceed M/2 and N/2
a = 20;
b = 20;

r = rect2D(M, N, a, b);
figure; imshow(r);
title('Q5: 2D Rect function between [-20, 20]');

%% ========================= Question 6/7 ========================= %%
% Take fft2 of functions n and r and display the spectrum

%% The cosine function
fft_n0 = fft2(n0);
figure; imshow(mat2gray(20*log10(abs(fft_n0)), [-600, 100]));
title('Q6: FFT of 2D cosine uo = 0, v0 = 0');

fft_n1 = fft2(n1);
figure; imshow(mat2gray(20*log10(abs(fft_n1)), [-600, 100]));
title('Q6: FFT of 2D cosine uo = M/4, v0 = 0');

fft_n2 = fft2(n2);
figure; imshow(mat2gray(20*log10(abs(fft_n2)), [-600, 100]));
title('Q6: FFT of 2D cosine uo = 0, v0 = N/2');

fft_n3 = fft2(n3);
figure; imshow(mat2gray(20*log10(abs(fft_n3)), [-600, 100]));
title('Q6: FFT of 2D cosine uo = M/4, v0 = N/2');

%% Center the spectrum and display cos
%u = 0 v = 0
fft_n0C = fftshift(fft_n0);
figure; imshow(mat2gray(20*log10(abs(fft_n0C)), [-600, 100]));
title('Q7: Centered spectrum uo = 0, v0 = 0');

figure;
k0 = surf(20*log10(abs(fft_n0C)));
k0.EdgeColor = 'none';

% u = M/4 v = 0
fft_n1C = fftshift(fft_n1);
figure; imshow(mat2gray(20*log10(abs(fft_n1C)), [-600, 100]));
title('Q7: Centered spectrum uo = M/4, v0 = 0');

figure;
k = surf(20*log10(abs(fft_n1C)));
k.EdgeColor = 'none';

% u = 0 v = N/2
fft_n2C = fftshift(fft_n2);
figure; imshow(mat2gray(20*log10(abs(fft_n2C)), [-600, 100]));
title('Q7: Centered spectrum uo = 0, v0 = N/2');

figure;
k2 = surf(20*log10(abs(fft_n2C)));
k2.EdgeColor = 'none';

% u = M/4 v = N/2
fft_n3C = fftshift(fft_n3);
figure; imshow(mat2gray(20*log10(abs(fft_n3C)), [-600, 100]));
title('Q7: Centered spectrum uo = M/4, v0 = N/2');

figure;
k3 = surf(20*log10(abs(fft_n3C)));
k3.EdgeColor = 'none';


%% The rect function
fft_r = fft2(r);

figure; imshow(mat2gray(20*log10(abs(fft_r))));
title('Q6: Uncentered Spectrum of 2D rect function');

figure; un = surf(20*log10(abs(fft_r)));
un.EdgeColor = 'none';
title('Q6: Plotted Uncentered Spectrum of 2D rect');

%% Center the rect fft, display before and after scaling
% Show top view and graphical view
figure;imshow(mat2gray(20*log10(abs(fftshift(fft_r)))));
title('Q7: Fourier transform of 2D rect top view');

figure; s = surf(20*log10(abs(fftshift(fft_r))));
s.EdgeColor = 'none';
title('Q7: Fourier transform of 2D rect');


%% ========================= Question 8 ========================= %%
% Create an ideal low pass filter and apply it to the fftshifted functions
% Show the idea low pass filter as an image

LPF_buf = zeros(size(img, 1), size(img, 2), 3);

% Set the cutoff diameter of the LPF
D = 5;
LPF_buf(:, :, 1) = createLPF(img, D);

D = 10;
LPF_buf(:, :, 2) = createLPF(img, D);

D = 20;
LPF_buf(:, :, 3) = createLPF(img, D);

% Display LPF as an image
LPF_buf = mat2gray(LPF_buf);
figure; imshow(mat2gray(LPF_buf(:, :, 1)));
title('Q8: Low pass filter with D = 5');

figure; montage(LPF_buf, 'Size', [1, 3]);
title('Q8: Low pass filter with D = 5, 10, 20');

%% ========================= Question 9 ========================= %%
% Apply this ideal lowpass filter to the image 
% Convolution in spatial domain = multiplication in Fourier domain
% We just need to convert to Fourier domain and element-wise multiply

% Convert to frequency domain and center the low frequencies with
% fftshift()
fft_img = fft2(img);
fft_cent = fftshift(fft_img);

% Create image buffer
L_fft = zeros(size(LPF_buf, 1), size(LPF_buf, 2), 3);

% Apply the low pass filter with different levels of filtering
L_fft(:, :, 1) = fft_cent .* LPF_buf(:, :, 1);
L_fft(:, :, 2) = fft_cent .* LPF_buf(:, :, 2);
L_fft(:, :, 3) = fft_cent .* LPF_buf(:, :, 3);

% Display
L_img = abs(ifft2(L_fft));
figure; montage(L_img, 'Size', [1, 3]);
title('Q9: Low pass filtered image at diameter D = 5, 10, 20');


%% ========================= Question 10 ========================= %%
% Design an ideal highpass filter and apply it to the image

% Create image volume buffers
HPF = zeros(size(img, 1), size(img, 2), 3); % To display high pass filters
H_fft = zeros(size(img, 1), size(img, 2), 3); % To hold filtered image in frequency domain
H_img = zeros(size(img, 1), size(img, 2), 3); % To display filtered images 

% Set the cutoff diameter and create the high pass filter
C = 5;
HPF(:, :, 1) = createHPF(img, C);

C = 10;
HPF(:, :, 2) = createHPF(img, C);

C = 20;
HPF(:, :, 3) = createHPF(img, C);

% Display the filter as an image
figure; montage(HPF, 'Size', [1, 3]);
title('Q10: High pass filters with diameter C = 5, 10, 20');

% Apply filter to the frequency domain image
H_fft(:, :, 1) = fft_cent .* HPF(:, :, 1);
H_fft(:, :, 2) = fft_cent .* HPF(:, :, 2);
H_fft(:, :, 3) = fft_cent .* HPF(:, :, 3);

% Change the image back to spatial domain and display
H_img = abs(ifft2(H_fft)); 
figure; montage(H_img, 'Size', [1, 3]);
title('Q10: High pass filtered image at diameter C = 5, 10, 20');




