%% ========================= Setup ========================= %%
%% ========================= Setup ========================= %%
% Setting up directories
addpath(genpath(pwd));

% Delete the file known as DEADJOE, only found in yale01 of CroppedYale
currFolder = pwd;
cd('CroppedYale\yaleB01');
delete DEADJOE;

cd(currFolder);
cd('CroppedYale');
folders = dir;


%% ========================= Question 1 ========================= %%
% For each subject, call calcDistances on that folder, then cd() back out
% and do it all again
for i = 3:size(folders, 1)
    folderNum = i-2;
    [d(i-2, :), p(i-2, :), avg(:, :, i-2)] = calcDistances(folders(i).name, folderNum);

    cd('..')
end

% Display montage of averaged images of all subjects
figure; 
montage(avg);
title('Q1: Averaged Images of all Subjects');

% plot 
% Euclidean on x axis
% Penrose on y axis
figure;
for jj = 1:size(d, 1)
    subplot(9, 5, jj);
    scatter(d(jj, :), p(jj, :));
    title(['Q1: Subject ' num2str(jj)]);
end

cd(currFolder);

%% ========================= Bonus ========================= %%
% Choose an image with an illumination
% The chosen illumination is A-005E-10
[img, ~] = getpgmraw('yaleB20_P00A-005E-10.pgm');
figure;
imshow(mat2gray(img));
title('Bonus: Chosen Subject: yaleB20_P00A-005E-10');

% Vectorize the image
imVec = reshape(img, [size(img, 1)*size(img, 2), 1]);

% Read 9 other poses at the same illumination and vectorize them
% The chosen illumination is A-005E-10
for i = 1:9
    [picBank(:, :, i), ~] = getpgmraw(['yaleB19_P0' num2str(i-1) 'A-005E-10.pgm']);
    picVec(:, i) = reshape(picBank(:, :, i), [size(picBank, 1)*size(picBank, 2), 1]);
end

% Display  basis subject and all the poses
figure;
imshow(mat2gray(picBank(:, :, 1)));
title('Bonus: Basis Subject: yaleB19_P00A-005E-10');

figure; 
montage(mat2gray(picBank));
title('Bonus: All Poses');

% Necessary dot products
% Image dotted with basis vectors
for i = 1:9
    dotImgJ(i) = dot(imVec, picVec(:, i));
end

% Basis vectors dotted with themselves
for j = 1:9
    for k = 1:9 
        dotJs(j, k) = dot(picVec(:, j), picVec(:, k));
    end
end
 
% Create system of equations with 9 variables and solve
syms C1 C2 C3 C4 C5 C6 C7 C8 C9
for n = 1:9
   eqn(n) = C1*dotJs(n,1) + C2*dotJs(n,2) + C3*dotJs(n,3) + C4*dotJs(n,4) + C5*dotJs(n,5) + ...
        C6*dotJs(n,6) + C7*dotJs(n,7) + C8*dotJs(n,8) + C9*dotJs(n, 9) == dotImgJ(n);  
end

sol = solve(eqn, [C1 C2 C3 C4 C5 C6 C7 C8 C9]);

% Extract coefficients
a = double(sol.C1);
b = double(sol.C2);
c = double(sol.C3);
d = double(sol.C4);
e = double(sol.C5);
f = double(sol.C6);
g = double(sol.C7);
h = double(sol.C8);
r = double(sol.C9);

% Reconstruct the image, then reshape and display
recon = a*picVec(:,1) + b*picVec(:,2) + c*picVec(:,3) + d*picVec(:,4) + e*picVec(:,5) +...
    f*picVec(:,6) + g*picVec(:,7) + h*picVec(:,8) + r*picVec(:,9);

reconImg = reshape(recon, [size(img, 1), size(img, 2)]);
figure; imshow(mat2gray(reconImg));
title('Bonus: Reconstructed Image');


