function [d, p, avg] = calcDistances(folder, folderNum)
% This functionc calculates the Euclidean and Penrose distances from the
% CroppedYale image database for all subjects and returns those values,
% along with the averaged image for each subject

cd(folder);

files = dir;

% Read in all images, store them in picBank
for i = 6:size(files, 1)
    try
        [picBank(:, :, i-5), maxgray(i)] = getpgmraw(files(i).name);
    catch
        disp(['Corrupt image folder number ', num2str(folderNum)]);
        picBank(:, :, i-5) = picBank(:, :, i-1-5);
    end
end

% convert to grayscale
picBank = mat2gray(picBank);

% Compute average image
avg = mean(picBank, 3);

% figure; 
% imshow(avg);
% title(['Mean image for folder ', num2str(folderNum)]);

% for ii = 1:size(pic, 3)
%    imshow(pic(:, :,ii));
%    hold on
%    pause(0.1);
% end

% Vectorize the averaged image, and the other images
avgPic = reshape(avg, [size(avg, 1)*size(avg, 2), 1]);

for ii = 1:size(picBank, 3)
    pic(:, ii) = reshape(picBank(:, :, ii), [size(picBank, 1)*size(picBank, 2), 1]);
end

%% Calculate Euclidean and Penrose distances

% Calculate Euclidean distance d
for jj = 1:size(pic, 2)
    d(jj) = sqrt(sum((avgPic - pic(:, jj)).^2, 1));
end

% Calculate Penrose distance

% v = 1/N * sum( (Iij - meanI)^2 ) variance of jth pixel
% p = sum( (I1 - I2)^2/var(j'th pixel) ) Penrose formula

% Calculate the difference between the (i, j) pixel and the variance of jth
% pixel 
% It has recently come to my attention I could simply have used var() for
% this
v = sum((pic - avgPic).^2, 2);
va = v ./ size(pic, 2); % variance of jth pixel

% Calculate Penrose distance from the average picture
for n = 1:size(pic, 2)
    p(n) = sum(((pic(:, n) - avgPic).^2) ./ va, 1);
end

% plot 
% Euclidean on x axis
% Penrose on y axis
% figure;
% scatter(d, p);
% title(['Q1: Scatter plot for folder ' num2str(folderNum)]);

end