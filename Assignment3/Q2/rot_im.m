function imr = rot_im(im, rad)
% This funcion takes an image im and performs a matrix rotation by rad
% radians. This is done by first padding the original image, and creating
% an image buffer. Then we rotate the image buffer and obtain the newly 
% rotated coordiantes. Using these new cordinates we can inverse transform
% them to the original position. If these fall into the range of our
% original image, we interpolate the image at these points, and reconstruct
% our image in the rotated buffer


% create padded image and initialize image to write to
padSize = 500;
bfi = padarray(im, [padSize, padSize], 0, 'both');
newIm = zeros(size(bfi, 1), size(bfi, 2)); %initialize new image
compareIm = newIm;

% figure; 
% imshow(bfi);
% title('Q2: Zero padded image');

% rotate the coordinates by number of rads
% store the rotated coordinates 
idx = 0;
for i = 1:size(im, 1)
   for j = 1:size(im, 2)
       idx = idx + 1;
       % One for x, one for y
       bufRow(idx) = i*cos(rad) - j*sin(rad)+padSize;
       bufCol(idx) = j*cos(rad) + i*sin(rad)+padSize;
   end
end

% reconstruct the rotated coordinates in the empty image
% set all the pixels to 1 to identify and verify the rotation was correct
% use floor() and ceil() to cover all pixel values (they will be decimals
% after the rotation)
for s = 1:idx
   newIm(floor(bufRow(s)), floor(bufCol(s))) = 1; 
   newIm(ceil(bufRow(s)), ceil(bufCol(s))) = 1; 
end

% display transformed coordinates
figure; 
imshow(newIm);
title('Q2: Transformation grid');
line([0 1300], [500,500], 'LineWidth', 1, 'Color', 'w');
line([500,500], [0, 1300], 'LineWidth', 1, 'Color', 'w');

% iterate through newIm, if a value of 1 is found inverse transform that
% coordinate back to the original locaton
% if that coordinate falls in the original image, interpolate and set the
% value of that pixel in newIm equal to the interpolation
c= 0;
for ii = 1:size(newIm, 1)
    for jj = 1:size(newIm, 2)
        
        % look for pixels equal to 1, those indices will be the ones for
        % our reconstructed, rotated image
        if(newIm(ii, jj) == 1)
           c = c + 1;
           nR(c) = ii;
           nC(c) = jj;
           
           % inverse rotate the image making sure to offset by padSize
           % we need our (ii, jj) to be from 500 to 800, with this offset,
           % at (1300, 1300) we will actually be at (800, 800), the end of
           % the original image **we are only inverse rotating the rotated
           % coordinates, which are 300x300**
           iRow = (ii-padSize)*cos(rad) + (jj-padSize)*sin(rad);
           iCol = (jj-padSize)*cos(rad) - (ii-padSize)*sin(rad);
           
           % if the inverse rotated pixel falls within our original image
           % range, interpolate the image to the rotated buffer
           if((iRow < 300 && iRow > 1) && (iCol < 300 && iCol > 1))
                newIm(ii, jj) = b_interp(im, iCol, iRow);
           end
        end
        
    end
end

%% Quiver plot
qRows = zeros(1300, 1300);
qCols = zeros(1300, 1300);

cen = floor(size(newIm/2, 1));

% Apply rotation matrix to the grid starting from negative to positive
% Assume (0, 0) is at the corner of rotation
for  n = 1:size(newIm, 1)
   for m = 1:size(newIm, 2)
       qRows(n, m) = (n-cen)*cos(rad) - (m-cen)*sin(rad) - (n-cen);
       qCols(n, m) = (m-cen)*cos(rad) + (n-cen)*sin(rad) - (m-cen);
   end
end


% Plot
figure;
quiver(qCols(1:50:end, 1:50:end), qRows(1:50:end, 1:50:end), 1);
ax = gca;
ax.XDir = 'Reverse';
title('Q2: Quiver plot');
% line([0 30], [15,15], 'LineWidth', 1, 'Color', 'k');
% line([15,15], [0, 30], 'LineWidth', 1, 'Color', 'k');

% return
imr = mat2gray(newIm);

end










