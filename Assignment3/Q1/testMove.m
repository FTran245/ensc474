function image = testMove(im, T1, T2)
% This function takes an image im and performs a translation of (T1, T2)
% This is done by first padding the image, then reconstructing the image at
% the new offset (T1, T2) by using our bilinear interpolaton


% find the image dimensions
nrows = size(im, 1);
ncols = size(im, 2);

% initialize the recreated image
image = zeros(nrows, ncols);

% extract decimal component 
dT1 = T1 - floor(T1);
dT2 = T2 - floor(T2);

% get pad size
padS = [abs(fix(T1)), abs(fix(T2))];

% create image buffer padImg
padImg = padarray(im, padS, 0, 'both');
padImg = zeros(size(padImg, 1), size(padImg, 2));

% reconstruct the image at the specified offset on the buffer
for i = padS(1):padS(1)+nrows
    for j = padS(2):padS(2)+ncols
        padImg(i+fix(T1)+1, j+fix(T2)+1) = b_interp(im, j+dT2-padS(2)+1, i+dT1-padS(1)+1);
    end
end

% display
% figure; 
% imshow(mat2gray(padImg));
% title('Q1: Translated Image by (50.5, 50.5)');

image = mat2gray(padImg);

%% IGNORE
% for i = 1:nrows
%    for j = 1:ncols
%       
%        if(i < T1 || j < T2)
%            image(i, j) = 0;
%        else
%             image(i, j) = b_interp(im, j - T2, i - T1); 
%        end
%    end
% end
% % reconstruct the image using interpolation 
% for ii = 1:nrows-1
%    for jj = 1:ncols-1
%       image(ii, jj) = b_interp(im, T1+jj, T2+ii); 
%    end
% end
%% IGNORE

end

