%% ========================= Setup ========================= %%
%% ========================= Setup ========================= %%
directory = pwd;
addpath(genpath(pwd));

% read in image, convert to grayscale, and crop too 300x300
img = imread('selfie.jpg');
img = rgb2gray(img);
im = img(150:end-33, 100:end-243);

% zero pad the original image
im = padarray(im, [1, 1], 0, 'both');

figure;
imshow(im);
title('300 x 300 image');

%% ========================= Question 1 ========================= %%
x = 150;
y = 150;
fx = b_interp(im, x, y);

T1 = 50.5;
T2 = 50.5;
image = testMove(im, T2, T1);

figure;
imshow(mat2gray(image));
title('Q1: Translated image by (50.5, 50.5) pixels with interpolation');


%% ========================= Question 2 ========================= %%

imr = rot_im(im, pi/6);
figure;
imshow(imr);
title('Q2: Rotated Image by pi/6');
line([0 1300], [500,500], 'LineWidth', 1, 'Color', 'w');
line([500,500], [0, 1300], 'LineWidth', 1, 'Color', 'w');


%% Rotations at different angles
% imr = rot_im(im, pi/3);
% imshow(imr);
% title('Q2: Rotated Image by pi/3');
% line([0 1300], [500,500], 'LineWidth', 1, 'Color', 'w');
% line([500,500], [0, 1300], 'LineWidth', 1, 'Color', 'w');
% 
% imr = rot_im(im, pi/2);
% imshow(imr);
% title('Q2: Rotated Image by pi/2');
% line([0 1300], [500,500], 'LineWidth', 1, 'Color', 'w');
% line([500,500], [0, 1300], 'LineWidth', 1, 'Color', 'w');
% 
% imr = rot_im(im, pi);
% imshow(imr);
% title('Q2: Rotated Image by pi');
% line([0 1300], [500,500], 'LineWidth', 1, 'Color', 'w');
% line([500,500], [0, 1300], 'LineWidth', 1, 'Color', 'w');

 
