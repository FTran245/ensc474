%% ========================= Setup ========================= %%
%% ========================= Setup ========================= %%
%add directories
%read in images and convert to grayscale, then resize and display

directory = pwd;
addpath(genpath(pwd));

%read in images
img1 = imread('mugshot1.jpg');
img2 = imread('toronto.jpg');
img1 = imrotate(img1, 270);

%convert to grayscale
img1_gr = rgb2gray(img1);
img2_gr = rgb2gray(img2);

figure; 
imshow(imresize(img1_gr, [600 600]));
title('Figure Q4.1: Original Mugshot (resized)');

figure; 
imshow(imresize(img2_gr, [600 600]));
title('Figure Q4.1: Original Non-Face Image (resized)');

%get image dimensions
y = size(img1_gr, 1);
x = size(img1_gr, 2);

yd = size(img2_gr, 1);
xd = size(img2_gr, 2);

%crop images

J = imresize(img1_gr, [600 601]);
Jtor = imresize(img2_gr, [600 601]);

J = imcrop(img1_gr, [x/3, y/3, 600, 601]);
Jtor = imcrop(img2_gr, [xd/3, yd/3, 600, 601]);

figure; imshow(J);
title('Figure Q4.1: Cropped mugshot');

figure; imshow(Jtor);
title('Figure Q4.1: Cropped non-face Image');

%% ========================= Question 4.1 ========================= %%
%Jtor -> a
%J -> b 
%proj =  (dot(a, b) / magnitude(a)^2 ) * a

[proj, dotProd]= computeProj(Jtor, J);
proj_img = reshape(proj, [602, 601]);
figure; imshow(proj_img);
title('Figure Q4.1: Projected Image');

%definition of orthogonal: if two vectors are perpendicular
%if the dot product = 0, then the vectors are orthogonal

%% ========================= Question 4.3/4.4/4.5 ========================= %%

%Read new mugshot image, rotate and crop
im2 = imread('mugshot.jpg');
im2 = rgb2gray(im2);
im2 = imrotate(im2, 270);
im3 = im2(200:end-350, 1:end-300);
im3 = imcrop(im2, [size(im2, 2)/3, size(im2, 1)/3, 600, 601]);

figure; imshow(im3);
title('Figure Q4.3: New Mugshot');

%vectorize the images
I = reshape(im3, [size(im3, 1)*size(im3, 2), 1]);
Jvec = reshape(J, [size(J, 1)*size(J, 2), 1]); 
Jtor_vec = reshape(Jtor, [size(Jtor, 1)*size(Jtor, 2), 1]); 

%dot(I, J)
I_J = dot(double(I), double(Jvec));

%dot(I, Jtor)
I_Jtor = dot(double(I), double(Jtor_vec));

%necessary dot products
f_dot_f = dot(double(Jvec), double(Jvec));
f_dot_nf = dot(double(Jvec), double(Jtor_vec));
nf_dot_nf = dot(double(Jtor_vec), double(Jtor_vec));

% the equations
% i.face = alpha*f.f + beta*n.f
% i.nonface = alpha*f.n + beta*n.n

%solve the system of equations
syms x y 
eqn1 = f_dot_f*x + f_dot_nf*y == I_J;
eqn2 = f_dot_nf*x + nf_dot_nf*y == I_Jtor;

sol = solve(eqn1, eqn2, [x, y]);

%cast 
alpha = double(sol.x);
beta = double(sol.y);

%reconstruct the image and reshape into a matrix
recon_vec = alpha*Jvec + beta*Jtor_vec;
recon = reshape(recon_vec, [size(J, 1), size(J, 2)]);

%display reconstructed image
figure;
imshow(mat2gray(recon));
title('Figure Q4.4: Reconstructed Image');

%the reconstruction is not at all close to the original image
%we can expect this because the original image is not in the vector space
%created by the two new images we chose

%% ========================= Question 4.6 ========================= %%
%other images
%read in, convert to grayscale, and crop
pen1 = imread('pen1.JPG');
pen2 = imread('pen2.JPG');
pen3 = imread('pen3.JPG');

pen1 = rgb2gray(pen1);
pen2 = rgb2gray(pen2);
pen3 = rgb2gray(pen3);

x = size(pen1, 1) - size(pen2, 1);
y = size(pen1, 2) - size(pen2, 2);

p = pen1(1:end - x, 1:end-y);

%Display
figure; 
imshow(imresize(pen2, [600 600]));
title('Figure Q4.6: Basis Image 1 (resized)');

figure; 
imshow(imresize(pen3, [600 600]));
title('Figure Q4.6: Basis Image 2 (resized)');

figure; 
imshow(imresize(pen1, [600 600]));
title('Figure Q4.6: Image to be Reconstructed (resized)');

% pImg = a*pen2 + b*pen3

%vectorize
p1 = reshape(p, [size(p, 1)*size(p, 2), 1]);
p2 = reshape(pen2, [size(pen2, 1)*size(pen2, 2), 1]);
p3 = reshape(pen3, [size(pen3, 1)*size(pen3, 2), 1]);

%dot products
p12 = dot(double(p1), double(p2));
p13 = dot(double(p1), double(p3));

p22 = dot(double(p2), double(p2));
p23 = dot(double(p2), double(p3));
p33 = dot(double(p3), double(p3));

%solve the system of equations
syms a b 
eqn3 = p22*a + p23*b == p12;
eqn4 = p23*a + p33*b == p13;

sol2 = solve(eqn3, eqn4, [a, b]);

%cast and reconstruct
c1 = double(sol2.a);
c2 = double(sol2.b);

pImg = c1*p2 + c2*p3;
pRecon = reshape(pImg, [size(pen2, 1), size(pen2, 2)]);

%compare images
figure;
imshow(imresize(pRecon, [600 600]));
title('Figure Q4.6: Reconstructed Image (resized)');






