%This function computes the projection of two vectors J along Jtor
%by first converting the images to a column vector,
%then applying the projection formula

function [proj, dotProd]= computeProj(Jtor, J)
%proj =  (dot(a, b) / magnitude(a)^2 ) * a

%vectorize matrices to MNx1 vectors
Jvec = reshape(J, [size(J, 1)*size(J, 2), 1]); 
Jtor_vec = reshape(Jtor, [size(Jtor, 1)*size(Jtor, 2), 1]); 

%compute dot product
dotAB = dot(double(Jvec), double(Jtor_vec), 1);
dotProd = dotAB;

%compute magnitude squared
magSqJtor_vec = norm(double(Jtor_vec)) ^ 2; 

%find the scaling factor
scalar = double(dotAB) / double(magSqJtor_vec);

%apply scaling factor to projected vector
proj = scalar*Jtor_vec;

end