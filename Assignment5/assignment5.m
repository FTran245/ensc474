%% ========================= Setup ========================= %%
%% ========================= Setup ========================= %%
addpath(genpath(pwd));

%% Read in mugshot, resize, and convert to grayscale
img = imread('selfie.jpg');
img = im2double(mat2gray(rgb2gray(img)));
img = imresize(img, [size(img, 1)/2, size(img, 2)/2]);

figure; imshow(mat2gray(img));
title('Q1: Original Image');

%% ========================= Question 1 ========================= %%
% Create Sobel mask
sobel = [0, 1, 0; 0, -2, 1; 0, 0, 0];
sobel = sobel';

con = applyFilter(img, sobel);

% Display edge detected image
figure; imshow(mat2gray(con));
title('Q1: Edge Detected Image with mat2gray()');

% Scale from 0 to 1 to preserve negative values
cont = mat2gray(con, [0, 1]);
figure; imshow(cont);
title('Q1: Edge Detected Image Scaled [0, 1]');

% Brighten the edges by mapping from 0 to 0.2
br = mat2gray(con, [0, 0.2]);
figure; imshow(br);
title('Q1: Edge Dectected Image Scaled [0, 0.2]');


%% ========================= Question 2 ========================= %%
% Laplacian enhancement mask from lecture along with some variations

% Create the enhancement filters
laplace = [0, -1, 0; -1, 5, -1; 0, -1, 0];
laplace = laplace';

laplace2 = [0, -1, 0; -1, 8, -1; 0, -1, 0];
laplace2 = laplace2';

laplace5 = ones(5, 5);
laplace5 = -laplace5;
laplace5(3, 3) = 24;
laplace5 = laplace5';

% Apply filters to image with convolution
en = applyFilter(img, laplace);
en2 = applyFilter(img, laplace2);
en5 = applyFilter(img, laplace5);

% Display
figure; imshow(mat2gray(en), [0, 1]);
title('Q2: Enhanced Image 3x3 Laplacian');

figure; imshow(mat2gray(en2), [0, 1]);
title('Q2: Enhanced Image 3x3 Laplacian 8');

figure; imshow((en5));
title('Q2: Enhanced Image 5x5 Laplacian');

figure; imshow(mat2gray(en5), [0, 1]);
title('Q2: Enhanced Image 5x5 Laplacian scaled [0, 1]');






